#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`recursive sorts`
:author: `FIL - FST - Univ. Lille.fr <http://portail.fil.univ-lille1.fr>`_
:date: 2016, september. Last revised: 2018, september

Some recursive sorting algorithms:

- quicksort
- mergesort

"""
from typing import Callable, TypeVar
from aplst import ApLst


T = TypeVar('T')


def compare(a: T, b: T) -> int:
    """
    return:
       - -1 if a < b
       -  1 if a > b
       -  0 if a = b
    precondition: a and b must be comparable with <
    exemples:

    $$$ compare(0, 1)
    -1
    $$$ compare('a', 'a')
    0
    $$$ compare((2, 1), (1, 2))
    1
    """
    if a < b:
        return -1
    elif a > b:
        return 1
    else:
        return 0


def length(li: ApLst) -> int:
    """
    return the length of li.

    precondition: none

    examples:

    $$$ length(ApLst())
    0
    $$$ length(ApLst(3, ApLst(1, ApLst(4, ApLst()))))
    3
    """
    if not li.is_empty():
        res = 1 + length(li.tail())
    else:
        res = 0
    return res


def native_to_list(li: list[T]) -> ApLst:
    """
    return a recursive list containing the same element of li.

    precondition: none

    examples:

    $$$ native_to_list([]).is_empty()
    True
    $$$ rec_lst = native_to_list([3, 1, 4, 1, 5])
    $$$ length(rec_lst)
    5
    $$$ rec_lst.head()
    3
    $$$ l = rec_lst.tail()
    $$$ l.head()
    1
    $$$ l = l.tail()
    $$$ l.head()
    4
    """
    if li != []:
        res = ApLst(li[0], native_to_list(li[1:]))
    else:
        res = ApLst()
    return res


def list_to_native(li: ApLst) -> list[T]:
    """
    return a native python list containing the same element of li.

    precondition: none

    examples:

    $$$ list_to_native(ApLst())
    []
    $$$ list_to_native(ApLst(3, ApLst(1, ApLst(4, ApLst(1, ApLst(5, ApLst()))))))
    [3, 1, 4, 1, 5]
    """
    res = []
    while not li.is_empty():
        res.append(li.head())
        li = li.tail()
    return res


def is_sorted(l: ApLst, comp: Callable[[T, T], int]=compare) -> bool:
    """
    return True if list l is sorted by ascending order
    and False otherwise.

    precondition: elements of l must be comparable
    exemples:

    $$$ is_sorted(native_to_list([1, 2, 3, 4]))
    True
    $$$ is_sorted(native_to_list([1, 2, 4, 3]))
    False
    """
    if l.is_empty():
        return True

    current = l.head()
    rest = l.tail()

    while not rest.is_empty():
        next_element = rest.head()
        if comp(current, next_element) > 0:
            return False
        current = next_element
        rest = rest.tail()

    return True
#     compteur = 0
#     while not l.is_empty and l.comp(l.head(), is_sorted(l.tail()) == -1):
#         compteur = compteur + 1
#     return compteur == length(l)


def split(l: ApLst) -> tuple[ApLst, ApLst]:
    """
    return a couple (l1,l2) of lists of equal length

    exemples:

    $$$ l = [3, 1, 4, 1, 5, 9, 2]
    $$$ l1, l2 = split(native_to_list(l))
    $$$ abs(length(l1) - length(l2)) <= 1
    True
    $$$ l3 = list_to_native(l1) + list_to_native(l2)
    $$$ len(l3) == len(l)
    True
    $$$ all(k in l for k in l3)
    True
    """
#     l1 = ApLst()
#     l2 = ApLst()
#     compteur = 0
#     while not l.is_empty():
#         if compteur % 2 == 0:
#             l1 = ApLst(l.head(), l1)
#         else:
#             l2 = ApLst(l.head(), l2)
#         l = l.tail()
#         compteur += 1
#     return (l1, l2)       
    if not l.is_empty():
        h1 = l.head()
        if not l.tail().is_empty():
            h2 = l.tail().head()
            r1, r2 = split(l.tail().tail())
            return ApLst(h1, r1), ApLst(h2, r2)
        else:
            return ApLst(h1,ApLst()),ApLst()
    else:
        return ApLst(), ApLst()

def merge(l1: ApLst, l2: ApLst,
          comp: Callable[[T, T], int]=compare) -> ApLst:
    """
    return a list containing all elements de l1 and l2.
    If l1 and l2 are sorted, so is the returned list.

    precondition: elements of l1 and l2 are comparable
    exemples:

    $$$ list_to_native(merge(native_to_list([1, 3, 4, 9]), native_to_list([1, 2, 5])))
    [1, 1, 2, 3, 4, 5, 9]
    """
    if (not l1.is_empty()) and (not l2.is_empty()):
        h1, h2 = l1.head(), l2.head()
        if comp(h1, h2) <= 0:
            res = ApLst(h1, merge(l1.tail(), l2))
        else:
            res = ApLst(h2, merge(l1, l2.tail()))
    else:
        if l1.is_empty():
            res = l2
        else:
            res = l1
    return res
        
#     res = []
#     if l1.is_empty() and l2.is_empty():
#         return res
#     elif l1.is_empty() and not l2.is_empty():
#         res.append(l2.tail())
#     elif not l1.is_empty() and l2.is_empty():
#         res.append(l1.tail())
#     else:
#         l1 = l1.head()
#         l2 = l2.head()
#         if compare(l1, l2) < 0:
#             res.append(l1)
#             l1 = l1.tail()
#             merge(l1, l2)
#         elif compare(l1, l2) > 0:
#             res.append(l2)
#             l2 = l2.tail()
#             merge(l1, l2)
#         else:
#             res.append(l1)
#             res.append(l2)
#             l1 = l1.tail()
#             l2 = l2.tail()
#             merge(l1, l2)


def mergesort(l: ApLst, comp: Callable[[T, T], int]=compare) -> ApLst:
    """
    return a new list containing elements of l sorted by ascending order.

    precondition: elements of l are comparable
    exemples:

    $$$ list_to_native(mergesort(native_to_list([3, 1, 4, 1, 5, 9, 2])))
    [1, 1, 2, 3, 4, 5, 9]
    $$$ import random
    $$$ n = random.randrange(20)
    $$$ l = native_to_list([random.randrange(20) for k in range(n)])
    $$$ l1 = mergesort(l)
    $$$ length(l1) == length(l)
    True
    $$$ is_sorted(l1)
    True
    """
    if l.is_empty() or l.tail().is_empty():
        return l
    else:
        l1, l2 = split(l)
        sorted_l1 = mergesort(l1, comp)
        sorted_l2 = mergesort(l2, comp)
        return merge(sorted_l1, sorted_l2, comp)


if (__name__ == '__main__'):
    import apl1test
    apl1test.testmod("merge_sort.py")



