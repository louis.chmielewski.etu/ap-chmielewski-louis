#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`analyse_tris` module
:author: FIL - Faculté des Sciences et Technologies - Univ. Lille <http://portail.fil.univ-lille1.fr>_
:date: janvier 2017
:dernières révisions: février 2018, février 2019

Analyse empirique des tris

"""
from random import shuffle
from typing import Callable
from compare import compare
from ap_decorators import count
from tris import *
from functools import cmp_to_key
import timeit
import matplotlib.pyplot as plt
from tris import liste_alea, tri_select

################################################
#  ANALYSE EMPIRIQUE DES TRIS                  #
################################################

# ajout d'un compteur à la fonction compare
compare = count(compare)

def analyser_tri(tri: Callable[[list[T], Callable[[T, T], int]], NoneType],
                 nbre_essais: int,
                 taille: int) -> float:
    """
    renvoie: le nombre moyen de comparaisons effectuées par l'algo tri
         pour trier des listes de taille t, la moyenne étant calculée
         sur n listes aléatoires.
    précondition: n > 0, t >= 0, la fonc
    """
    res = 0
    for i in range(nbre_essais):
        compare.counter = 0
        l = [k for k in range(taille)]
        shuffle(l)
        tri(l, compare)
        res += compare.counter
    return res / nbre_essais

def tri_sort(l:list[int], cmp):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    l.sort(key = cmp_to_key(cmp))


if (__name__ == '__main__'):
    from matplotlib import pyplot as plt
    
    # Calcul de nombres moyens de comparaison pour des listes
    # de tailles comprises entre 0 et TAILLE_MAX
    NB_ESSAIS = 50
    TAILLE_MAX = 100
    c_select = [0.0] * (TAILLE_MAX + 1)
    c_insert = [0.0] * (TAILLE_MAX + 1)
    c_sort = [0.0] * (TAILLE_MAX + 1)
    
    for t in range(TAILLE_MAX + 1):
        c_select[t] = analyser_tri(tri_select, 1, t)
        # inutile de moyenner pour le tri par sélection
        c_insert[t] = analyser_tri(tri_insert, NB_ESSAIS, t)
        c_sort[t] = analyser_tri(tri_sort, NB_ESSAIS, t)

    # Sauvegarde des données calculées dans un fichier au format CSV
    prem_ligne = 'taille;"tri séléction";"tri insertion"\n'
    ligne = '{:3d};{:8.2f};{:8.2f}\n'
    with open('analyse_tris.csv', 'wt', encoding='utf8') as sortie:
        sortie.write(prem_ligne)
        for t in range(TAILLE_MAX + 1):
            sortie.write(ligne.format(t,
                                      c_select[t],
                                      c_insert[t]))

    # Représentation graphique
#     plt.plot(list(range(TAILLE_MAX + 1)), c_select, 'b.', label='Tri sélection')
#     plt.plot(list(range(TAILLE_MAX + 1)), c_insert, 'r.', label='Tri insertion')
#     plt.plot(list(range(TAILLE_MAX + 1)), c_sort, 'g.', label='Tri sort')
#     plt.title('Tris : nbre de comparaisons')
#     plt.legend()
#     plt.xlabel('n = taille des listes')
#     plt.ylabel('c(n) = nbre de comparaisons')
#     plt.savefig('tris_nbcomp.png')
#     plt.show()

# Cas du tri par selection
# >>> timeit.timeit(stmt='tri_select', setup = f'from tris import liste_alea, tri_select; l=liste_alea({101})', number = 5000)
# 4.4625019654631615e-05

longueurs = liste_alea(101)

# Mesurer le temps d'exécution pour chaque longueur de liste
temps_executions = []
for longueur in longueurs:
    # Exécuter timeit pour mesurer le temps d'exécution du tri_select avec une liste de longueur donnée
    temps_execution = timeit.timeit(stmt='tri_select(l)', 
                                    setup=f'from tris import liste_alea, tri_select; l=liste_alea({longueur})', 
                                    number=5000)
    temps_executions.append(temps_execution)

# Tracer la courbe des temps mesurés en fonction de la longueur des listes
plt.plot(longueurs, temps_executions, 'g.')
plt.title('Temps d\'exécution de tri_select en fonction de la longueur des listes')
plt.xlabel('Longueur de la liste')
plt.ylabel('Temps d\'exécution (secondes)')
plt.show()