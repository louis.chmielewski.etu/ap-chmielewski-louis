#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`tris` module
:author: FIL - Faculté des Sciences et Technologies -
         Univ. Lille <http://portail.fil.univ-lille1.fr>_
:date: 2015, january
:dernière révision: mars 2024


Tris de listes

- tri par sélection
- tri par insertion

"""

from random import shuffle
import matplotlib.pyplot as plt

def liste_alea(n:int) -> list[int]:
    """
    Renvoie une liste de 0 à n-1 mélangé
    Précondition : aucune
    Exemple(s) :
    $$$ 

    """
    res = list(range(n))
    shuffle(res)
    return res