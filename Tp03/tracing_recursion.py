# Louis Chmielewski
# Tp03
# 31/01/2024

from ap_decorators import trace

def fact(n: int) -> int:
    """
    Renvoie n!
    Condition d'utilisation : n >= 0
    """
    if n == 0:
       res = 1
    else:
       res = n * fact(n - 1)
    return res

def somme_rec(a:int, b:int) ->int:
    """
    Renvoie la somme de 2 entiers par récurrence
    Précondition : 
    Exemple(s) :
    $$$ somme_rec(1, 1)
    2
    """
    if a > 0:
        res = somme_rec(a-1, b+1)
    elif a == 0:
        res = b
    return res

def binomial(n:int, p:int) ->int:
    """
    Renvoie le coefficient binomial de manière récursive 
    Précondition :  𝑛 ≥ 𝑝 ≥ 0
    Exemple(s) :
    $$$ binomial(5,2)
    10
    """
    if p == 0 or p == n:
        return 1
    else:
        return binomial(n - 1, p - 1) + binomial(n - 1, p)
@trace
def palindrome(mot:str)->bool:
    """
    Renvoie True si le mot est un palindrome 
    Précondition : 
    Exemple(s) :
    $$$ palindrome('radar') 
    True
    $$$ palindrome('bonjur') 
    False
    """
    if len(mot)==1 or len(mot)==0:
        return True
    else:
        if mot[0]==mot[-1]:
            return palindrome(mot[1:-1])
        else:
            return False
        
# Trace de binomial(5,2)
-> binomial((5, 2), {})
... -> binomial((4, 1), {})
...... -> binomial((3, 0), {})
...... <- 1
...... -> binomial((3, 1), {})
......... -> binomial((2, 0), {})
......... <- 1
......... -> binomial((2, 1), {})
............ -> binomial((1, 0), {})
............ <- 1
............ -> binomial((1, 1), {})
............ <- 1
......... <- 2
...... <- 3
... <- 4
... -> binomial((4, 2), {})
...... -> binomial((3, 1), {})
......... -> binomial((2, 0), {})
......... <- 1
......... -> binomial((2, 1), {})
............ -> binomial((1, 0), {})
............ <- 1
............ -> binomial((1, 1), {})
............ <- 1
......... <- 2
...... <- 3
...... -> binomial((3, 2), {})
......... -> binomial((2, 1), {})
............ -> binomial((1, 0), {})
............ <- 1
............ -> binomial((1, 1), {})
............ <- 1
......... <- 2
......... -> binomial((2, 2), {})
......... <- 1
...... <- 3
... <- 6
 <- 10
10

# Trace de palindrome('radar')
 -> palindrome(('radar',), {})
... -> palindrome(('ada',), {})
...... -> palindrome(('d',), {})
...... <- True
... <- True
 <- True
True