# Louis Chmielewski
# Tp03
# 31/01/2024

from ap_decorators import*
@count
def fibo(n:int)->int:
    """
    Renvoie le terme fn de la suite de Fibonacci.
    Précondition : aucune
    Exemple(s) :
    $$$ fibo(10)
    55
    $$$ fibo(0)
    0
    $$$ fibo(1)
    1
    $$$ fibo(2)
    1
    """
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo(n-1) + fibo(n-2)
    
# fibo(40) prend du temps a s'éxécuter
