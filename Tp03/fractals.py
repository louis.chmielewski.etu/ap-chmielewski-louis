# Louis Chmielewski
# Tp03
# 31/01/2024

import turtle

def zigzag():
    """
    Renvoie un zigzag
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    for i in range(10):
        turtle.left(45)
        turtle.forward(100)
        turtle.right(90)
        turtle.forward(100)
        turtle.left(45)

# La relation entre la longueur de tracé l à l'ordre n est que l est divisé par 3

def courbe_von_koch(l: int or float, n: int):
    """
    Dessine la courbe de von kosh d'ordre n
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    turtle.speed(1000)
    if n == 0:
        turtle.forward(l)
    else:
        courbe_von_koch(l/3, n-1)
        turtle.left(60)
        courbe_von_koch(l/3, n-1)
        turtle.right(120)
        courbe_von_koch(l/3, n-1)
        turtle.left(60)
        courbe_von_koch(l/3, n-1)
        
def flocon(l: int or float, n: int):
    """
    Dessine le flocon de Von Koch d'ordre n tel que le montre la figure ci-dessus lorsque n = 4
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    for i in range (0, 6) :
        courbe_von_koch(l, n)
        turtle.left(60)
        courbe_von_koch(l, n)
        turtle.right(120)

# la relation entre la longueur de tracé est de l/4

def courbe_cesaro(l:int or float, n:int):
    """
    Dessine la courbe de cesaro d'ordre n
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    turtle.speed(5000)
    l = l/4
    if n == 0:
        turtle.forward(l*4)
    else:
        courbe_cesaro(l, n-1)
        turtle.left(85)
        courbe_cesaro(l, n-1)
        turtle.right(170)
        courbe_cesaro(l, n-1)
        turtle.left(85)
        courbe_cesaro(l, n-1)
        
def carre_cesaro(l:int or float, n:int):
    """
    Renvoie la courbe cesaro d'ordre 5
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    for i in range (0, 4):
        courbe_cesaro(l, n-1)
        turtle.left(90)
        
# La relation entre le tracé à l'ordre n et l'ordre n+1 et l/2
        
def triangle_sierpinski(l:int or float, n:int):
    """
    Renvoie le triangle de sierpinski d'ordre n
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    turtle.speed(100000)
    if n == 0 :
        for i in range (0, 3):
            turtle.forward(l)
            turtle.left(120)
    else:
        triangle_sierpinski(l/2, n-1)
        turtle.forward(l/2)
        triangle_sierpinski(l/2, n-1)
        turtle.penup()
        turtle.left(120)
        turtle.forward(l/2)
        turtle.right(120)
        turtle.pendown()
        triangle_sierpinski(l/2, n-1)
        turtle.penup()
        turtle.right(120)
        turtle.forward(l/2)
        turtle.left(120)
        turtle.pendown()
        
        
    
    