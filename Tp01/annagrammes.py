# TP Annagrammes
# Louis CHMIELEWSKI
# 15/01/2024

# Méthode split

# 1.
# ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# ['la méthod', ' split ', 'st parfois bi', 'n util', '']
# ['la m', 'thode split est parfois bien utile']
# ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
# empty separator
# ['la méthode ', ' est parfois bien utile']

# 2.
# La méthode split transforme la chaine en liste et sépare les
# éléments de la liste avec l'argument de split tout en enlevant cet argument

# 3.
# Oui, elle enlève l'argument de split

# Méthode join

# 1.
# 'laméthodesplitestparfoisbienutile'
# 'la méthode split est parfois bien utile'
# 'la;méthode;split;est;parfois;bien;utile'
# 'la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'
# la
# méthode
# split
# est
# parfois
# bien
# utile
# 'la méthode split est parfois bien utile'
# 'l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'
# 0 argument given
# int found instead str instance

# 2.
# la méthode join permet de réunir les éléments entre eux, en intercalant la chaine passé entre les éléments
# pour une chaine c'est entre chaque lettre, pour une liste entre chaque élément

# 3.
# oui

# 4.
def join(s:str, l:list[str])->str:
    """
    Renvoie une chaîne de caractères construite en concaténant toutes les chaînes de l en intercalant s
    Précondition : aucune
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'
    """
    res = ''
    for i in range(len(l)):
        if i == len(l) - 1:
            res = res + l[i]
        else:
            res = res + l[i] + s
    return res

# Méthode sort

# 1.
# ['e', 'i', 'l', 'm', 'n', 'o', 'o', 't']
# [' ', ' ', ' ', ' ', ' ', "'", '.', 'J', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'f', 'i', 'i', 'j', 'j', 'l', 'm', 'n', 'o', 's', 't', 'u', 'é', 'û']
# les caractères de l sont rangés dans l'ordre alphabétique

# 2.
# on ne peut pas trier des types différents entre eux

# Une fonction sort pour les chaines

# 1.
def sort(s:str)->str:
    """
    Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.
    Précondition: aucune
    Exemples:
    $$$ sort('timoleon')
    'eilmnoot'
    """
    res = ''.join(s.split())
    res = ''.join(sorted(res))
    return res

# Annagrammes

def sont_anagrammes(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.
    Précondition: aucune
    Exemples:
    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    False
    """
    i = 0
    s1 = sort(s1)
    s2 = sort(s2)
    while i < len(s1) and i < len(s2) and s1[i] == s2[i]:
        i = i + 1
    return i == len(s1) == len(s2)

def sont_anagrammes1(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.
    Précondition: aucune
    Exemples:
    $$$ sont_anagrammes1('orange', 'organe')
    True
    $$$ sont_anagrammes1('orange','Organe')
    False
    $$$ sont_anagrammes1('orange','orangé')
    False
    """
    res1 = {}
    res2 = {}
    for lettre1 in s1:
        res1[lettre1] = res1.get(lettre1, 0) + 1
    for lettre2 in s2:
        res2[lettre2] = res2.get(lettre2, 0) + 1
    return res1 == res2

def sont_anagrammes2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.
    Précondition: aucune
    Exemples:
    $$$ sont_anagrammes2('orange', 'organe')
    True
    $$$ sont_anagrammes2('orange','Organe')
    False
    $$$ sont_anagrammes2('orange','orangé')
    False
    """
    res1 = []
    res2 = []
    for lettre1 in set(s1):
        res1.append([lettre1, s1.count(lettre1)])
    for lettre2 in set(s2):
        res2.append([lettre2, s2.count(lettre2)])
    return res1 == res2
    
# Casse et accentuation

EQUIV_NON_ACCENTUE = {'à': 'a','â': 'a','ä': 'a','ç': 'c','é': 'e','è': 'e','ê': 'e','ë': 'e','î': 'i','ï': 'i','ô': 'o','ö': 'o','ù': 'u','û': 'u','ü': 'u','ÿ': 'y'}

def bas_casse_sans_accent(chaine:str) -> str:
    """
    Renvoie une chaîne de caractères identiques à celle passée
    en paramètre sauf pour les lettres majuscules et les lettres
    accentuées qui sont converties en leur équivalent minuscule non accentué
    Précondition : aucune
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    """
    res = ''
    for i in range(len(chaine)):
        if chaine[i] in EQUIV_NON_ACCENTUE:
            res = res + EQUIV_NON_ACCENTUE[chaine[i]]
        else:
            res = res + chaine[i]
    res = res.lower()
    return res

def sont_anagrammes3(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.
    Précondition: aucune
    Exemples:
    $$$ sont_anagrammes3('orange', 'organe')
    True
    $$$ sont_anagrammes3('orange','Organe')
    True
    $$$ sont_anagrammes3('orange','orangé')
    True
    $$$ sont_anagrammes3('orangee','orangé')
    False
    """
    s1 = bas_casse_sans_accent(s1)
    s2 = bas_casse_sans_accent(s2)
    return sont_anagrammes(s1, s2)

from lexique import LEXIQUE
# Le lexique

# 1.
# Le lexique contient 139719 mots
# Ce lexique ne contient pas de doublons


# Anagrammes d'un mot : première méthode

# 1.
def anagrammes1(mot: str) -> list[str]:
    """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.
    Précondition: aucune
    Exemples:
    $$$ anagrammes1('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes1('info')
    ['foin']
    $$$ anagrammes1('Calbuth')
    []
    """
    res = []
    for i in LEXIQUE:
        if sont_anagrammes3(mot, i):
            res.append(i)
    return res

# 2.
#['chien', 'chiné', 'niche', 'niché']


# Anagrammes d'un mot : deuxième méthode

# 1.
# Car le mot en clé n'est pas mit en valeurs de cette même clé

# 2.
def cle(mot:str) -> str:
    """
    à_remplacer_par_ce_que_fait_la_fonction
    Précondition : 
    Exemple(s) :
    $$$ cle('Orangé')
    'aegnor'
    """
    return sort(bas_casse_sans_accent(mot))

# 1.
ANAGRAMMES = {}
for mot in LEXIQUE:
    cle_mot = cle(mot)
    if cle_mot not in ANAGRAMMES:
        ANAGRAMMES[cle_mot] = [mot]
    else:
        ANAGRAMMES[cle_mot].append(mot)

# 1.
def anagrammes2(mot):
    """
    Renvoie les anagrammes du mot en paramètres à partir du dictionnaire
    Précondition : aucune
    Exemple(s) :
    $$$ anagrammes2('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes2('info')
    ['foin']
    $$$ anagrammes2('Calbuth')
    []
    """
    cle_mot = cle(mot)
    if cle_mot in ANAGRAMMES:
        return ANAGRAMMES[cle_mot]
    else:
        return []

# 2.
#['chien', 'chiné', 'niche', 'niché']

# 1.
# Méthode 1
#for mot in LEXIQUE[:30]:
#    res1 = anagrammes1(mot)
#    print(f"Nombre d'anagrammes pour {mot}: {len(res1)}")

# Méthode 2
for mot in LEXIQUE[:30]:
    res2 = anagrammes2(mot)
    print(f"Nombre d'anagrammes pour {mot}: {len(res2)}")
            

# On remarque que la deuxième méthode est beaucoup plus rapide

# Phrases d'anagrammes

# 1.
def anagrammes_phrase(phrase:str) -> list[str]:
    """
    Renvoie la liste des phrases obtenues en remplaçant chacun des mots par leurs anagrammes
    Précondition : aucune
    Exemple(s) :
    $$$ anagrammes_phrase('Mange ton orange')
    ['mange ont onagre', 'mange ont orange', 'mange ont orangé', 'mange ont organe', 'mange ont rongea', 'mange ton onagre', 'mange ton orange', 'mange ton orangé', 'mange ton organe', 'mange ton rongea', 'mangé ont onagre', 'mangé ont orange', 'mangé ont orangé', 'mangé ont organe', 'mangé ont rongea', 'mangé ton onagre', 'mangé ton orange', 'mangé ton orangé', 'mangé ton organe', 'mangé ton rongea']
    """
    fonction non réussi
            
        
#     liste_mot = phrase.split()
#     i = 0
#     while i < len(liste_mot):
#         for i in liste_mot:
#             mot_v = [anagrammes1(mot[i]) for i in liste_mot]
    
    
