import minesweeper
from minesweeper import Minesweeper
import graphicalboard

game = Minesweeper(20, 20, 99)
graphicalboard.create(game)