#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`fonctions` module : fonctions principal

:author: Louis Chmielewski

:date: 2024 mars

"""
import sys
sys.path.append("src")
sys.path.append("assets")
from PIL import Image, ImageDraw
from bloc import Bloc
import manip_couleurs

image = input("Entrez le chemin de votre fichier : ")
def manipulation_image(image: str)->None:
    """
    Ouvre une image et la manipule pour la fonction decoupe_image
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    image = Image.open(image)
    image_rgb = image.convert('RGB')
    image.size = (width, height)
    image_rgb.pixel_hl = (0,0)
    image_rgb.pixel_lr = (width, height)
    draw = ImageDraw.Draw(image)

def decoupe_image(image: "Bloc", ordre: int) -> Bloc:
    """
    Decoupe un bloc en quatre bloc "ordre" fois 
    Précondition : ordre >= 0
    Exemple(s) :
    $$$ 

    """
    if ordre != 0:
        blocs = en_4_blocs(image)
        for bloc in blocs:
            decoupe_image(bloc, ordre - 1)
        
        if sont_4_blocs_uniformes_proches(blocs):
            couleur_mid = moyenne_couleur(blocs)
            pixel_hl = image.pixel_hl
            pixel_lr = image.pixel_lr
            draw.rectangle((pixel_hl, pixel_lr), fill=couleur_mid)
        else:
            pixel_hl = image.pixel_hl
            pixel_lr = image.pixel_lr
            draw.rectangle((pixel_hl, pixel_lr))
    else:
        couleur_mid = moyenne_couleur(image)
        pixel_hl = image.pixel_hl
        pixel_lr = image.pixel_lr
        draw.rectangle((pixel_hl, pixel_lr), fill=couleur_mid)
        
        
#     if ordre != 0:
#         blocs = en_4_blocs(image)
#         for bloc in blocs:
#             decoupe_image(bloc, ordre - 1, draw)
#         
#         if sont_4_blocs_uniformes_proches(blocs):
#             couleur_mid = moyenne_couleur(blocs)
#             pixel_hl = image.pixel_hl
#             pixel_lr = image.pixel_lr
#             draw.rectangle((pixel_hl, pixel_lr), fill=couleur_mid)
#         else:
#             pixel_hl = image.pixel_hl
#             pixel_lr = image.pixel_lr
#             draw.rectangle((pixel_hl, pixel_lr))
#     else:
#         couleur_mid = moyenne_couleur(image)
#         pixel_hl = image.pixel_hl
#         pixel_lr = image.pixel_lr
#         draw.rectangle((pixel_hl, pixel_lr), fill=couleur_mid)
        
        
        
#     if ordre  != 0:
#         en_4_blocs(image)
#         decoupe_image(*arg, ordre - 1)
#         if sont_4_blocs_uniformes_proches([bloc1, bloc2, bloc3, bloc4]):
#             couleur_mid = moyenne_couleur([bloc1, bloc2, bloc3, bloc4])
#             pixel_hl = image.getpixel(image.pixel_hl(bloc1))
#             pixel_lr = image.getpixel(image.pixel_lr(bloc4))
#             draw.rectangle((pixel_hl),(pixel_lr), fill(couleur_mid))
#         else:
#             pixel_hl = image.getpixel(image.pixel_hl(bloc1))
#             pixel_lr = image.getpixel(image.pixel_lr(bloc4))
#             draw.rectangle((pixel_hl),(pixel_lr))
#     else:
#         couleur_mid = moyenne_couleur(bloc)
#         pixel_hl = image.getpixel(image.pixel_hl(image))
#         pixel_lr = image.getpixel(image.pixel_lr(image))
#         draw.rectangle((pixel_hl),(pixel_lr), fill(couleur_mid))         
            
            
            