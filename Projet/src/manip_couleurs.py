#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`fonctions` module : fonctions pour la résolution du projet

:author: Louis Chmielewski

:date: 2024 mars

"""

def moyenne_couleur_bloc(liste: list[tuple[int,int,int]]) -> tuple:
    """
    Renvoie la moyenne de la couleur d'un bloc
    Précondition : aucune
    Exemple(s) :
    $$$ moyenne_couleur_bloc([(1,2,3),(4,5,6),(7,8,9)])
    (4,5,6)
    $$$ moyenne_couleur_bloc([(1,2,3),(4,5,6),(7,8,9),(10,11,12),(13,14,15)])
    (7,8,9)
    """
    total_r = 0
    total_g = 0
    total_b = 0
    for couleur in liste:
        total_r += couleur[0]
        total_g += couleur[1]
        total_b += couleur[2]
    nombre_blocs = len(liste)
    moyenne_r = total_r / nombre_blocs
    moyenne_g = total_g / nombre_blocs
    moyenne_b = total_b / nombre_blocs
    return (moyenne_r, moyenne_g, moyenne_b)

def est_couleur_proche(couleur1 : tuple[int,int,int], couleur2 : tuple[int,int,int]) -> bool:
    """
    Renvoie True ssi 2 couleurs sont proches, sinon False
    Précondition : len(couleur1) == len(couleur2) == 3
    Exemple(s) :
    $$$ est_couleur_proche((120, 130, 140), (125, 125, 137))
    True
    $$$ est_couleur_proche((12, 13, 14), (125, 125, 137))
    False
    """
    res = True
    for i in range(len(couleur1)):
        if abs(couleur1[i] - couleur2[i]) > 15:
            res = False
    return res
        
    