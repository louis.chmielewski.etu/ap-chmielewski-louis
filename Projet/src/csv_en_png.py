#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`fonctions` module : convertisseur de fichiers csv pour obtenir en rgb

:author: Louis Chmielewski

:date: 2024 mars

"""
import sys
sys.path.append("assets")
import csv
from PIL import Image, ImageDraw

# Récupère la valeur maximale compris dans la 4eme colonne pour obtenir la dimension max de l'image
def lit_dimensions(fichier_csv: str)->tuple[int, int]:
    """
    Renvoie un tuple de 2 valueurs signifiant la taille de l'image du fichier csv
    Précondition : fichier_csv un fichier csv
    Exemple(s) :
    $$$ 

    """
    max_y2 = 0
    with open(fichier_csv, newline='') as csvfile:
        csv_reader = csv.reader(csvfile)
        for ligne in csv_reader:
            if len(ligne) == 7:
                x1, y1, x2, y2, r, g, b = map(int, ligne)
                if y2 > max_y2:
                    max_y2 = y2
    return (max_y2, max_y2)

fichier_csv = input(" Entrez le chemin de votre fichier : ")

width, height = lit_dimensions(fichier_csv)
image = Image.new("RGB", (width, height), "black")
draw = ImageDraw.Draw(image)

with open(fichier_csv, newline='') as csvfile:
    csv_reader = csv.reader(csvfile)
    for ligne in csv_reader:
        if len(ligne) == 7:
            x1, y1, x2, y2, r, g, b = map(int, ligne) 
            draw.rectangle([x1, y1, x2, y2], fill=(r, g, b))

image.show()