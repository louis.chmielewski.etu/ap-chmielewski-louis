#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`association` module : un module pour la modélisation des "Bloc"s

:author: Louis Chmielewski

:date: 2024 mars

"""

from PIL import Image, ImageDraw
from manip_couleurs import *
    
class Bloc:
    """
    Classe d'un "Bloc" d'une image en pixel
    Exemple(s) :
    $$$ self.pixel_hl = (0, 0)
    $$$ self.pixel_lr = (10, 10)
    $$$ self.color = (255, 0, 0)
    $$$ self.bloc = "Bloc"(self.pixel_hl, self.pixel_lr, self.color)
    """
    

    def __init__(self, pixel_hl: tuple[int, int], pixel_lr: tuple[int, int], color:tuple[int, int, int]):
        """
        Initialise le type "Bloc" à la valeur de l'image 
        """
        # 
        self.pixel_hl = pixel_hl
        self.pixel_lr = pixel_lr
        self.color = color
        
    def __eq__(self, other):
        """
        Renvoie True si 2 images sont identiques au pixel près
        Précondition : aucune
        Exemple(s) :
        $$$ 

        """
        return self.bloc == other.bloc
    
    def est_bloc_uniforme(liste_bloc: list["Bloc", "Bloc", "Bloc", "Bloc"])->bool:
        """
        Renvoie True si le "Bloc" est un "Bloc" uniforme, c'est à dire si les 4 couleurs du "Bloc" sont proches 
        Précondition : aucune
        Exemple(s) :
        $$$ 

        """
        return est_couleur_proche(moyenne_couleur_bloc(liste_bloc[0].color), moyenne_couleur_bloc(liste_bloc[1].color)) and est_couleur_proche(moyenne_couleur_bloc(liste_bloc[0].color), moyenne_couleur_bloc(liste_bloc[2].color)) and est_couleur_proche(moyenne_couleur_bloc(liste_bloc[0].color), moyenne_couleur_bloc(liste_bloc[3].color))
# est_couleur_proche(moyenne_couleur_bloc(liste_bloc[0].color), moyenne_couleur_bloc(liste_bloc[1].color))
# est_couleur_proche(moyenne_couleur_bloc(liste_bloc[0].color), moyenne_couleur_bloc(liste_bloc[2].color))
# est_couleur_proche(moyenne_couleur_bloc(liste_bloc[0].color), moyenne_couleur_bloc(liste_bloc[3].color))
    
    def est_bloc_non_uniforme(liste_bloc: list["Bloc", "Bloc", "Bloc", "Bloc"])-> bool:
        """
        Renvoie True si le "Bloc" n'est pas uniforme, c'est à dire
        si il peut être divisé en 4 "Bloc"s
        Précondition : aucune
        Exemple(s) :
        $$$ 

        """
        return not(est_bloc_uniforme(liste_bloc))
    
    def sont_4_blocs_uniformes_proches(liste_bloc: list["Bloc", "Bloc", "Bloc", "Bloc"])-> bool:
        """
        Renvoie True si les 4 "Bloc"s uniformes sont proches
        Précondition : aucune
        Exemple(s) :
        $$$ sont_4_blocs_uniformes_proches(bloc1, "Bloc"2, "Bloc"3, "Bloc"4)
        True
        $$$ sont_4_blocs_uniformes_proches(bloc1, "Bloc"2, "Bloc"3, "Bloc"4)
        False
        """
        res = False
        if all(est_bloc_uniforme(liste_bloc)):
            for i in range(1,len(liste_bloc)-1):
                if est_couleur_proche(bloc1, "Bloc"[i]):
                    res = True
        return res
            
            
    def en_4_blocs(bloc: "Bloc")->"Bloc":
        """
        Divise un "Bloc" en 4 "Bloc"s
        Précondition : aucune
        Exemple(s) :
        $$$ 

        """
        # Obtenez les coordonnées du coin supérieur gauche et du coin inférieur droit du "Bloc"
        x_hl, y_hl = Bloc.pixel_hl
        x_lr, y_lr = Bloc.pixel_lr
    
        # Calculez les coordonnées du milieu du "Bloc"
        x_milieu = (x_hl + x_lr) // 2
        y_milieu = (y_hl + y_lr) // 2
    
        # Obtenez les couleurs des pixels pour les 4 coins du "Bloc"
        couleur_bloc1 = Bloc.getpixel((x_hl, y_hl))
        couleur_bloc2 = Bloc.getpixel((x_milieu, y_hl))
        couleur_bloc3 = Bloc.getpixel((x_hl, y_milieu))
        couleur_bloc4 = Bloc.getpixel((x_milieu, y_milieu))
    
        # Créez les 4 nouveaux "Bloc"s à partir des pixels obtenus
        Bloc1 = Bloc((x_hl, y_hl), (x_milieu, y_milieu))
        Bloc1.pixel_couleur = couleur_bloc1
    
        Bloc2 = Bloc((x_milieu, y_hl), (x_lr, y_milieu))
        Bloc2.pixel_couleur = couleur_bloc2
    
        Bloc3 = Bloc((x_hl, y_milieu), (x_milieu, y_lr))
        Bloc3.pixel_couleur = couleur_bloc3
    
        Bloc4 = Bloc((x_milieu, y_milieu), (x_lr, y_lr))
        Bloc4.pixel_couleur = couleur_bloc4
    
        # Retourner une liste contenant les 4 nouveaux "Bloc"s
        return [bloc1, Bloc2, Bloc3, Bloc4]
    
    
    
#         "Bloc"1 = pixel_hl = image.getpixel((0, 0)) and pixel_lr = image.getpixel((largeur/2, hauteur/2))
#         "Bloc"2 = pixel_hl = image.getpixel((largeur/2, 0)) and pixel_lr = image.getpixel((largeur, hauteur/2))
#         "Bloc"3 = pixel_hl = image.getpixel((0, hauteur/2)) and pixel_lr = image.getpixel((largeur/2, hauteur))
#         "Bloc"4 = pixel_hl = image.getpixel((largeur/2, hauteur/2)) and pixel_lr = image.getpixel((largeur, hauteur))
#         new_bloc = [bloc1, "Bloc"2, "Bloc"3, "Bloc"4]
#         return new_bloc
    
        
    
        
        
    