- - -
title : Projet
author : Louis Chmielewski
- - -

# journal
- 11/03 : organisation de la résolution du projet
- 11/03 : création du fichier fonction.py et réalisation de la la fonction moyenne_couleur_bloc

- 15/03 : réalisation de la fonction est_couleur_proche
- 15/03 : réalisation de la méthode __init__ pour la classe Bloc

- 17/03 : réalisation de la fonction est_bloc_uniforme pour la classe bloc
- 17/03 : réalisation de la fonction est_bloc_non_uniforme pour la classe bloc
- 17/03 : réalisation de la fonction sont_4_blocs_uniformes_proches pour la classe bloc (début)

- 18/03 : réalisation de la fonction sont_4_blocs_uniformes_proches pour la classe bloc (fin)

- 20/03 : redéfinition de la fonction est_bloc_uniforme de la class Bloc

- 22/03 : création du fichier utilisable en ligne de commande, se nommant logiciel_lcom.py
- 22/03 : réalisation de la fonction charger image dans logiciel_lcom.py 

- 24/03 : petite modification de la méthode init pour la class bloc
- 24/03 : début réalisation de la fonction découpe_image

- 26/03 : création du script de conversion de fichier csv en fichier png
- 26/03 : corrections d'erreurs dans bloc.py
- 26/03 : réalisation de la fonction decoupe_image

- 27/03 : redéfinition de la fonction decoupe image
- 27/03 : modification du fichier csv_en_png

- 28/03 : modification apportés sur une partie de l'ensemble des fichiers

- 29/03 : projet terminé avec quelques erreurs 
-> problèmes avec la spécification du type des paramètres pour les paramètres de type Bloc
-> problèmes avec l'import de fichier/fonction

- 01/04 : dernière modification avant rendu final
- 01/01 : rendu final (12h40)



