#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`fonctions` module : logiciel en ligne de commande

:author: Louis Chmielewski

:date: 2024 mars

"""
import sys
sys.path.append("src")
sys.path.append("assets")
import numpy as np
from PIL import *
from csv_en_png import *

def charger_image(im: str):
    """
    Charger une image depuis un fichier PNG ou CSV
    Précondition : im est le chemin d'un fichier
    Exemple(s) :
    $$$ 

    """
    im = Image.open(input("Entrez le chemin du fichier : "))
    im_rgb = im.convert('RGB')
    if im.endswith('.png'):
        return manipulation_image(im)
    elif im.endswith('.csv'):
        return csv_en_png(im)
    else:
        raise ValueError("Erreur, veuillez recommencer.")

def traitement_image(image):
    """
    Traitement de l'image avec l'algorithme.
    Précondition : aucune
    Exemple(s) :
    $$$ 

    """
    ordre = int(input("Veuillez entrer l'ordre à laquelle vous voulez traiter l'image"))
    im_rgb = decoupe_image(image, ordre)
    return im_rgb

def enregistre_image(image):
    """
    Enregistre l'image traiter
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    im_rgb.save("image.png", "PNG")

def main():
    """
    Fonction principale du fichier
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    charger_image()
    traitement_image()
    enregistre_image()

if __name__ == "__main__":
    main()

