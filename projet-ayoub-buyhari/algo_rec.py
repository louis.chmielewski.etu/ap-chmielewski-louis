from Bloc import Bloc
from Decoupage import *
from PIL import Image , ImageDraw
from enregistrement_lecture_image import *


def image_rec(image_: str, ordre: int):
    if ordre != 0:
        bloc = image_to_block(image_)
        sous_blocs = divise_bloc(image_, bloc, ordre)  
        image = Image.open(image_).convert("RGB")
        draw = ImageDraw.Draw(image)
        for sous_bloc in sous_blocs:
            px_hg = sous_bloc.px_hg
            px_bd = sous_bloc.px_bd
            couleur = sous_bloc.couleur
            draw.rectangle([px_hg, px_bd], fill=couleur)
        return divise_bloc(image_, bloc, ordre) 
    else:
        bloc = image_to_block(image_)
        couleur = couleur_moyenne_image(image_) 
        bloc.couleur = couleur
        image = Image.open(image_).convert("RGB")
        draw = ImageDraw.Draw(image)
        px_hg = bloc.px_hg
        px_bd = bloc.px_bd
        couleur = bloc.couleur
        draw.rectangle([px_hg, px_bd], fill=couleur)
        return bloc