# Louis Chmielewski
# Tp04
# 07/02/2024

from etudiant import Etudiant
from date import Date

def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    return not False in seq_bool

def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    return True in seq_bool

def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(int(nip), nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

L_ETUDIANTS = charge_fichier_etudiants('etudiants.csv')
COURTE_LISTE = L_ETUDIANTS[0:11]

def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de fiches d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    return all(isinstance(i,Etudiant) for i in x)

NBRE_ETUDIANTS = 603
NIP = 42204529
# 48874005 : Aurélie FERNANDEZ
        
ETUDIANT_MOINS20 = [L_ETUDIANTS[i] for i in range(NBRE_ETUDIANTS) if L_ETUDIANTS[i].naissance > Date(2,2,2004)]

def ensemble_des_formations(liste: list[Etudiant]) -> set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    return {L_ETUDIANTS[i].formation for i in range(len(liste))}


def occurrence_prenom(liste:list[Etudiant]) -> dict():
    """
    Renvoie le nombre d'occurrences de chaque prénom
    Précondition : aucune
    Exemple(s) :
    $$$ occurrence_prenom(L_ETUDIANTS)['Louis']
    2
    """
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res

# Camille : 2 Alexandre : 3
# On effectue un seul parcours de la liste

# 198 prénom différents

# Margot : 9

#res = occurrence_prenom(L_ETUDIANTS)
#for prenom, occurrence in res.items():
#    if occurrence > 7:
#print(occurrence, prenom)

def occurrences_nip(liste:list[Etudiant]) -> bool:
    """
    Renvoie True si et seulement tout les nip sont différents
    Précondition : aucune
    Exemple(s) :
    $$$ occurrences_nip(COURTE_LISTE)
    True
    """
    res = {}
    for etud in liste:
        nip = etud.nip
        if nip in res:
            res[nip] = res[nip] + 1
        else:
            res[nip] = 1
    for i in res:
        if res[nip] != 1:
            return False
    return True

def nombre_etu_par_formation(liste:list[Etudiant]) -> bool:
    """
    Renvoie le nombre d'étudiants de chaque formation de la liste d'étudiants en paramètres
    Précondition : aucune
    Exemple(s) :
    $$$ nombre_etu_par_formation(COURTE_LISTE)
    {'MI': 11, 'PEIP': 11, 'MIASHS': 11, 'LICAM': 11}
    """
    res = ensemble_des_formations(liste)
    res_final = {}
    for etud in liste:
        formation = etud.formation
        for formation in res:
            res_final[formation] = res_final.get(formation, 0) + 1
    return res_final

def liste_formation(liste: list[Etudiant], form: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants de la formation ``form``

    Précondition:  liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ l_MI = liste_formation(COURTE_LISTE, 'MI')
    $$$ len(l_MI)
    7
    $$$ type(l_MI[1]) == Etudiant
    True
    $$$ len(liste_formation(L_ETUDIANTS, 'INFO'))
    0
    """
    return [liste[i] for i in range(0, len(liste)) if liste[i].formation == form]

#res = ensemble_des_formations(liste)
#    res_final = []
#    for etud in liste:
#        formation = etud.formation
#        for formation in res:
#            if etud.formation == form:
#                res_final.append(etud)
#    return res_final